package com.example.sign_up;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class RegisterActivity extends AppCompatActivity {
    public static int counter;
    public static final String EXTRA_TEXT = "com.example.application.example.EXTRA_TEXT";
    public static final String EXTRA_TEXT2 = "com.example.application.example.EXTRA_TEXT2";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        counter++;

        Button button = (Button) findViewById(R.id.button4);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                EditText ed = (EditText) findViewById(R.id.editText3);
                EditText ed2 = (EditText) findViewById(R.id.editText6);
                EditText ed3 = (EditText) findViewById(R.id.editText5);
                String text = ed.getText().toString();
                String text2 = ed2.getText().toString();
                String text3 = ed3.getText().toString();
                if (!text.isEmpty() && !text2.isEmpty() && !text3.isEmpty()) {
                    Intent intent = new Intent(v.getContext(), MainActivity.class);
                    intent.putExtra(EXTRA_TEXT, text);
                    intent.putExtra(EXTRA_TEXT2, text2);
                    intent.putExtra("int_value_register", counter);
                    startActivity(intent);
                }

            }
        });
    }

}
