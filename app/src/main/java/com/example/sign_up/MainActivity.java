package com.example.sign_up;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    public static int counter,counter2,counter3;
    public static final String EXTRA_TEXT = "com.example.application.example.EXTRA_TEXT";
    public static final String EXTRA_TEXT2 = "com.example.application.example.EXTRA_TEXT2";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        counter++;
        TextView tv = (TextView) findViewById(R.id.textView);
        TextView tv2 = (TextView) findViewById(R.id.textView2);
        TextView tv3 = (TextView) findViewById(R.id.textView11);

        Intent intent = getIntent();
        int  number = intent.getIntExtra("int_value_register",0);
        int  number2 = intent.getIntExtra("int_value_profile",0);
        if (number>0)
            counter2++;
        if (number2>0)
            counter3++;
        tv.setText("The onCreate was called: " + counter + " times!");
        tv2.setText("Number of calls from register page: "+counter2);
        tv3.setText("Number of calls from profile page: "+counter3);

        Button button,button2;
        button = (Button) findViewById(R.id.button);
        button2 = (Button) findViewById(R.id.button2);

        button.setOnClickListener(this);
        button2.setOnClickListener(this);
    }
    @Override
    public void onClick(View v){

        switch(v.getId()){
            case R.id.button:
                EditText ed = (EditText) findViewById(R.id.editText);
                EditText ed2 = (EditText) findViewById(R.id.editText2);
                String text = ed.getText().toString();
                String text2 = ed2.getText().toString();
                if (!text.isEmpty() && !text2.isEmpty()) {
                    Intent intent = new Intent(this, ProfileActivity.class);
                    intent.putExtra(EXTRA_TEXT,text);
                    intent.putExtra(EXTRA_TEXT2,text2);
                    intent.putExtra("int_value_main",counter);
                    startActivity(intent);
                    break;
                }
                else
                    break;
            case R.id.button2:
                    Intent intent2 = new Intent(this, RegisterActivity.class);
                    startActivity(intent2);
                    break;
        }

    }

    @Override
    protected void onResume(){
        super.onResume();


    }

}
